import turtle
import pandas
from turtle import Turtle, Screen

FONT = ("Arial", 10, "normal")
FONT2 = ("Arial", 50, "normal")
ALIGNMENT = "center"

screen = Screen()
screen.title("El mejor país del mundo")
screen.setup(width=725, height=491)
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)
screen.tracer(0)

data = pandas.read_csv("50_states.csv")
with open("states_to_learn.csv", "w") as lern:
    lern_gud = lern.write("")
state_list = data["state"].to_list()
states = data["state"]
correct = []
game = True
tony = 0
# TODO 1. Convert the guess to Title Case √
# TODO 2. Check if the guess is among the 50 states √
# TODO 3. Write correct guesses onto the map √
# TODO 4. Use a loop to allow the user to keep guessing √
# TODO 5. Record the correct guesses in a list √
# TODO 6. Keep track of the score √

#MAIN LOGIC HERE
while game:

    if tony > 0:
        title = f"{len(correct)} / 50 States Correct"
    else:
        title = "Guess Box"

    answer_state = screen.textinput(title=title, prompt="Guess a state name...")
    answer = answer_state.title()

    if answer == "Exit":
        break

    if answer and answer in states.to_list():
        tony = 1
        state = data[data.state == answer]
        x_list = state.x.to_list()
        y_list = state.y.to_list()
        tort = Turtle()
        tort.up()
        tort.hideturtle()
        # print(f"X Value: {x_list[0]}\nY Value: {y_list[0]}")

        if answer not in correct:
            # print(x, y)
            tort.goto(x_list[0], y_list[0])
            # tort.goto(int(state.x), int(state.y))
            tort.write(f"{answer}", font=FONT, align=ALIGNMENT)
            correct.append(answer)

    if len(correct) == 50:
        win = Turtle()
        win.hideturtle()
        win.up()
        win.color("red")
        win.write("You win!", font=FONT2, align=ALIGNMENT)
        print("You win!")
        game = False

        # print(answer)

# states_to_learn.csv
# for every state in correct that is not in original data set, save them to a csv
missed_dict = {
    "missed": [state for state in state_list if state not in correct]
}

if len(missed_dict["missed"]) > 0:
    to_learn = pandas.DataFrame(missed_dict)
    to_learn.to_csv("states_to_learn.csv")

screen.bye()


screen.listen()
screen.onkey(key="!", fun=screen.bye)

screen.mainloop()